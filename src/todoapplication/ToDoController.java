package todoapplication;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import javafx.event.EventHandler;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.input.MouseEvent;

public class ToDoController {
	
	private final ToDoModel model;
	private final ToDoView view;
	private int toDoIndex = 0;

	
	
	
	protected ToDoController(ToDoModel model, ToDoView view) {
		this.model = model;
		this.view = view;
		
		model.ReadFromFile();
		UpdateListView();
		
		
		
		
		view.btnErstellen.setOnAction(e -> {	
			
			if(TextFeldValidator() == false) {
				Alert a = new Alert(AlertType.ERROR, "Bitte alle Felder ausfüllen");
				a.showAndWait();
				return;
		
			}
			String titel = view.inputTitel.getText();
			String beschreibung = view.inputBeschreibung.getText();
			String dueDate = view.inputDueDate.getText();
			String ort = view.inputOrt.getText();
			
			
			if(DateTimeValidator(dueDate) == true) {
			
				DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
				LocalDateTime dateTime = LocalDateTime.parse(dueDate, formatter);
				
				ToDo item = new ToDo(titel,beschreibung,dateTime,ort);
				model.AddItem(item);
				
				Alert a = new Alert(AlertType.INFORMATION, "Notiz wurde erfolgreich gespeichert!");
				a.showAndWait();
				TextFelderLeeren();
				UpdateListView();
			}
			else {
				Alert a = new Alert(AlertType.ERROR, "Datumsformat muss yyyy-MM-dd HH:mm sein");
				a.showAndWait();
			}
			
			
			
			
			
		});
		
		
		view.btnAktualisieren.setOnAction(e -> {
			
			if(TextFeldValidator() == false) {
				Alert a = new Alert(AlertType.ERROR, "Bitte alle Felder ausfüllen");
				a.showAndWait();
				return;
		
			}
			

			String titel = view.inputTitel.getText();
			String beschreibung = view.inputBeschreibung.getText();
			String dueDate = view.inputDueDate.getText();
			String ort = view.inputOrt.getText();
			
			if(DateTimeValidator(dueDate) == true) {
				
				DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
				LocalDateTime dateTime = LocalDateTime.parse(dueDate, formatter);
			
				ToDo itemUpdate = new ToDo(titel,beschreibung,dateTime,ort);
				model.UpdateItem(toDoIndex, itemUpdate);
				
				Alert a = new Alert(AlertType.INFORMATION, "Notiz wurde erfolgreich aktualisiert!");
				a.showAndWait();
				TextFelderLeeren();
				UpdateListView();
			
			}
			else {
				Alert a = new Alert(AlertType.ERROR, "Datumsformat muss yyyy-MM-dd HH:mm sein");
				a.showAndWait();
			}
			
			
		
			
		});
		
		view.btnLoeschen.setOnAction(e ->{
			
			if(TextFeldValidator() == false) {
				Alert a = new Alert(AlertType.ERROR,"Felder sind leer, es gibt nichts zum Löschen :)");
				a.showAndWait();
				return;
			}
			
			model.DeleteItem(toDoIndex); 
			
			Alert a = new Alert(AlertType.INFORMATION,"Notiz wurde erfolgreich gelöscht!");
			a.showAndWait();
			TextFelderLeeren();
			UpdateListView();
			
		});
		
		view.btnNaechste.setOnAction(e ->{
			
			if(toDoIndex >= model.GetCount()-1) {
				return;
			}
			
			// der Zähler erhöht sich um eine weitere Zahl
			toDoIndex++;
			ToDo item = model.GetItem(toDoIndex);
			
			ShowItem(item);
			
			if(toDoIndex == model.GetCount()-1) {
				view.btnNaechste.setDisable(true);
			}
			
			if(toDoIndex > 0) {
				view.btnZurueck.setDisable(false);
			}
			
			view.listView.getSelectionModel().select(toDoIndex);
			
			
		});
		
		view.btnZurueck.setOnAction(e ->{
			
			if (toDoIndex  <= 0) { 
				return ;
			}
			
			// der Zähler substrahiert den Wert um eine Stelle
			toDoIndex--;
			ToDo item = model.GetItem(toDoIndex);
			ShowItem(item);
			
			if(toDoIndex == 0) {
				view.btnZurueck.setDisable(true);
			}
			
			if(toDoIndex < model.GetCount()-1) {
				view.btnNaechste.setDisable(false);
			}
			view.listView.getSelectionModel().select(toDoIndex);
			
		});
		
		
		view.listView.setOnMouseClicked(new EventHandler<MouseEvent>() {

	        @Override
	        public void handle(MouseEvent event) {
	        	int index =  view.listView.getSelectionModel().getSelectedIndex();
	        	toDoIndex  = index;
				ToDo item = model.GetItem(index);
				ShowItem(item);
				
				if(toDoIndex == 0) {
					view.btnZurueck.setDisable(true);
				}
				else {
					view.btnZurueck.setDisable(false);
				}
				
				if(toDoIndex == model.GetCount()-1) {
					view.btnNaechste.setDisable(true);
				}
				else {
					view.btnNaechste.setDisable(false);
				}	
	        }
	        
	        
		});
			
	}
	
	public void ShowItem(ToDo item) {
		
		view.inputTitel.setText(item.getTitel());
		view.inputBeschreibung.setText(item.getBeschreibung());
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
		String dueDate = item.getDueDate().format(formatter);
		view.inputDueDate.setText(dueDate);
		view.inputOrt.setText(item.getOrt());
	}
	
	public boolean DateTimeValidator(String dueDate) {
		
		try {
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
			LocalDateTime dateTime = LocalDateTime.parse(dueDate, formatter);
		}
		catch(Exception e) {
			return false;		
		}
		
		return true;
		
	}
	
	public boolean TextFeldValidator() {
		
		if(view.inputTitel.getText() == "") {
			return false;
		}
		if(view.inputBeschreibung.getText() == "") {
			return false;
		}
		if(view.inputDueDate.getText() == "") {
			return false;
		}
		if(view.inputOrt.getText() == "") {
			return false;
		}
		return true;
		
	}
	
	public void TextFelderLeeren() {
		
		view.inputTitel.setText("");
		view.inputBeschreibung.setText("");
		view.inputDueDate.setText("");
		view.inputOrt.setText("");
		
	}
	public void UpdateListView() {
		model.GetAll();
		view.listView.getItems().clear();
		for (ToDo todo : model.GetAll()) {
			view.listView.getItems().add(todo.getTitel());
		}
		
	}
	
}


	
	


