package todoapplication;

import java.io.Serializable;
import java.time.LocalDateTime;

public class ToDo implements Serializable{
	
	
	private static final long serialVersionUID = 1L;
	private String Titel;
	private String Beschreibung;
	private LocalDateTime DueDate;
	private String Ort;
	
	public ToDo( String Titel, String Beschreibung, LocalDateTime DueDate, String Ort) {	
		this.Titel= Titel;
		this.Beschreibung = Beschreibung;
		this.DueDate = DueDate;
		this.Ort = Ort;
	}
	
	public String getTitel() {
		return Titel;
	}
	
	public String getBeschreibung() {
		return Beschreibung;
	}
	
	public LocalDateTime getDueDate() {
		return DueDate;
	}
	
	public String getOrt() {
		return Ort;
	}
	
	public void  SetTitle (String Titel) {
		this.Titel = Titel;
	}
	
	public void SetBeschreibung (String Beschreibung) {
		this.Beschreibung = Beschreibung;
	}
	
	public void setDueDate ( LocalDateTime DueDate) {
		this.DueDate = DueDate;
	}
	
	public void setOrt( String Ort) {
		this.Ort = Ort;
	}
	
	//Test//
}
