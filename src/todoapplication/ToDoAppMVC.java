package todoapplication;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class ToDoAppMVC extends Application{
	
	private ToDoModel model;
	private ToDoView view;
	private ToDoController controller;

	public static void main(String[] args) {
		launch(args);

	}
	@Override 
	public void start(Stage primaryStage) throws Exception {
		model = new ToDoModel();
		view = new ToDoView(primaryStage,model);
		controller = new ToDoController(model, view);
		
		
		view.start();
	}

}


/*

Attributte :

Titel :String
Beschreibung : String
Due Date: Datentyp localdate , ist kein String 
Ort: String
Typ von ToDo : ist es Freizeit, Schule, Gruppenarbeiten, Projekt;



The VMC Model 


View :

Application Layout 
keep later interactions in mind 
Displöay most important information 
most important controls


Model:

Representation of essential data 
define needed data structures 
define essential functionality 
leave space for later iterations 


Controller:

How user will interact with the view ?
Plan event handling , properties, bindings 

*/