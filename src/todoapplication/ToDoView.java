package todoapplication;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class ToDoView {
	
	private final ToDoModel model;
	protected Label LblTitle;
	protected Label LblBeschreibung;
	protected Label LblDueDate;
	protected Label LblOrt;
	protected Button btnErstellen;
	protected Button btnAktualisieren;
	protected Button btnLoeschen;
	protected Button btnNaechste;
	protected Button btnZurueck;
	protected TextField inputTitel;
	protected TextArea inputBeschreibung;
	protected TextField inputDueDate;
	protected TextField inputOrt;
	protected Menu menu;
	protected MenuBar menuBar;
	protected ListView listView;

	
	
	
	
	
	
	private Stage stage;
	
	protected ToDoView (Stage stage, ToDoModel model) {
		this.stage = stage;
		this.model = model;
		
		stage.setTitle("ToDo App - Miniprojekt Nr.1 -Esteban Peña");
		
		
		
		
		//Labels fuer die Felder
		LblTitle = new Label("Titel: ");
		LblBeschreibung = new Label("Beschreibung: ");
		LblDueDate = new Label("Due Date: ");
		LblOrt = new Label("Ort: ");
		
		//TextField für die Inputs 
		inputTitel = new TextField();
		inputBeschreibung = new TextArea();
		inputBeschreibung.setPrefHeight(45.5);
		inputBeschreibung.setPrefWidth(20.5);
		inputDueDate = new TextField();
		inputDueDate.setPromptText("yyyy-MM-dd HH:mm");
		inputOrt = new TextField();
		
		//Buttons für die Funktionen 
		btnErstellen = new Button("Notiz erstellen");
		btnAktualisieren = new Button("Notiz aktualisieren");
		btnLoeschen = new Button("Noitz löschen");
		btnNaechste = new Button("Nächste");
		btnZurueck = new Button("Zurück");
		
		//Adding Übersicht 
		listView = new ListView();
		
		
		
		
		//Layout-Erstellung und controls, rows hinzufügen 
		GridPane root = new GridPane();
		root.setMinWidth(1000);
		root.setHgap(10);
		root.setVgap(10);
		root.setAlignment(Pos.CENTER);
		
		//Hinzufüge der Eingabefelder und Buttons 
		root.add(LblTitle, 0, 1);root.add(inputTitel, 2, 1);
		root.add(LblBeschreibung, 0, 2);root.add(inputBeschreibung, 2, 2);
		root.add(LblDueDate, 0, 3);root.add(inputDueDate, 2, 3);
		root.add(LblOrt, 0, 4);root.add(inputOrt, 2, 4);
		root.add(btnErstellen, 3, 1);
		root.add(btnAktualisieren, 3, 2);
		root.add(btnLoeschen, 3, 3);
		root.add(btnZurueck, 5, 0);
		root.add(btnNaechste, 6, 0);
		root.add(listView,2,5);
		
		
		
		
		
		

		Scene scene = new Scene(root);
		scene.getStylesheets().add(getClass().getResource("ToDo.css").toExternalForm());
		stage.setScene(scene);
		stage.setHeight(800);
		stage.setWidth(800);
	
	
	}
	
	
	
	public void  start() {
		stage.show();
		
	}
	
	public Stage getStage() {
		return stage;
	
	}

}
