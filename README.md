# ToDoApplication 

**DE:**
Software Engineering Miniprojekt Nr.1 - ToDo App mit Java, GUI wurde mit JavaFX realisiert (hardcoded)
Dieses Applikation wurde als Einzelarbeit entwickelt ( Esteban Andres P.Z)


**EN:**
Software Engineering minproject nr.1 - a simple to-Do App with Java, GUI with JavaFX (hardcoded)
These App has been wirtten by myself( Esteban Andres P.Z)

**Funktionen:**
Die Applikation verfügt über folgende Funktionen:

- Notiz als ToDo Item zu erstellen.
- Erstellte Notiz kann nach Speicherung geändert und danach aktualisiert werden.
- Erstellte Notizen können gelöscht werden.
- Mithilfe der Funktion bei den Knöpfen "zurück" und "weiter" kann zwischen den erstellten Notizen navigiert werden.
- Eine Übersicht über die erstellten Notizen wird unterhalb der Eingabefelder angezeigt.
- In der Übersicht kann navigiert werden, ebenso können die einzelne Notizen ausgewählt werden, diese werden dann mithilfe der Eingabefelder angezeigt. Die Übersichtliste zeigt ebenso auf welche Notiz man sich derzeit befindet. Die Notizen können dann ebenfalls aktualisiert oder gelöscht werden.
- Die Notizen werden im txt-Format in der Datei "Todo.txt" gespeichert.Daten werden automatisch in der Textdatei gespeichert und beim Öffnen des Programms automatisch abgerufen.Die Textdatei wird ebenso automatisch aktualisiert beim Löschen oder Aktualisieren der Notizen.
- Zusätzlich wurde mit CSS einen Hintergrund hinzugefügt und kleinen optischen Anpassungen bei den Knöpfen wurden ebenfalls getätigt






